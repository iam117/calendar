var app = angular.module("calendarApp", ['ui.bootstrap', 'ngAnimate']);

app.controller("CalendarController", function ($scope) {
    
    $scope.localSync = function() { // Функция синхронизации с localStorage
        localStorage.setItem('eventData', JSON.stringify($scope.eventData));
        $scope.eventData = JSON.parse(localStorage.getItem('eventData'));
    };
    
    if (!localStorage.getItem('eventData')) { // Если в localstorage нет данных
        $scope.eventData = {
            events: [
                // Добавляем всемирные праздники
                { international: true, fullDay: true, title: "New Year", description: "", dateTime: moment("01-01", "MM-DD")},
                { international: true, fullDay: true, title: "St. Valentine's Day", description: "", dateTime: moment("02-14", "MM-DD")},
                { international: true, fullDay: true, title: "Women's Day", description: "", dateTime: moment("03-08", "MM-DD")},
                { international: true, fullDay: true, title: "Fool's Day", description: "", dateTime: moment("04-01", "MM-DD")},
                { international: true, fullDay: true, title: "Family Day", description: "", dateTime: moment("05-15", "MM-DD")},
                { international: true, fullDay: true, title: "Children Protection Day", description: "", dateTime: moment("06-01", "MM-DD")},
                { international: true, fullDay: true, title: "International Chess day", description: "", dateTime: moment("07-20", "MM-DD")},
                { international: true, fullDay: true, title: "International Youth Day", description: "", dateTime: moment("08-12", "MM-DD")},
                { international: true, fullDay: true, title: "Day of Knowledge", description: "", dateTime: moment("09-01", "MM-DD")},
                { international: true, fullDay: true, title: "International Music Day", description: "", dateTime: moment("10-01", "MM-DD")},
                { international: true, fullDay: true, title: "International Information Day", description: "", dateTime: moment("11-26", "MM-DD")},
                { international: true, fullDay: true, title: "International Human Rights Day", description: "", dateTime: moment("12-10", "MM-DD")},
                { international: true, fullDay: true, title: "Christmas", description: "", dateTime: moment("12-25", "MM-DD")},
                
            ]
        };
        
        $scope.localSync();
        
        
    } else {
        $scope.eventData = JSON.parse(localStorage.getItem('eventData'));
    }
    
    
    
    $scope.showCalendar = true;
    $scope.showEventsPage = false;
    $scope.showNewEventPage = false;
    $scope.showEditPage = false;
    
    // Создание календаря
    
    $scope.month = moment();    // Хранит данные о текущем месяце 
    $scope.start = moment();    // Переменная для вычисления первого дня календаря
    $scope.start.date(1);        // Присваиваем значение 1-го дня  текущего месяца.
    $scope.start.weekday(0).add(1, "d");    // Присваиваем значение 1-го дня 1-й недели месяца и добавляем 1 день, т.к. по стандартам moment.js первый день недели - воскресенье.
    
    $scope.buildMonth = function(start, month) { 
        $scope.weeks = [];
        var done = false;
        var date = start.clone();
        var monthIndex = date.month();   // Индекс используемого месяца для последующей проверки.
        var count = 0;                   // Кол-во недель в месяце.
        
        while (!done) {
            $scope.weeks.push({ days: $scope.buildWeek( date.clone(), month ) });
            date.add(1, "w");     // Добавляем к текущей дате одну неделю
            done = count++ > 2 && monthIndex !== date.month();   // Если после 4-й недели месяц меняется , завершаем цикл.
            monthIndex = date.month();
        }
        
    };
    
    $scope.buildWeek = function(date, month) {
        var days = [];
        for (var i = 0; i < 7; i++) {
            days.push({
                name: date.format("ddd"), // Mon, Tue, Wed...
                number: date.date(),      // Число месяца
                isWeekend: i > 4,
                isCurrentMonth: date.month() === month.month(),
                isToday: date.isSame(new Date(), "day"),
                date: date,
                events: []
            });
            date = date.clone(); 
            date.add(1, "d");
        }
        return days;
    };
    
    
    
    
    
    $scope.pushEvents = function() { //Внесение событий в объекты дней
        
            //Выбираем события текущего месяца
        $scope.thisMonthEvents = [];
        var currentMonth = moment().format("YYYY-MM");
        for (var i = 0; i < $scope.eventData.events.length; i++) {
            var eventMonth = moment($scope.eventData.events[i].dateTime).format("YYYY-MM");
            if (eventMonth === currentMonth) {
                $scope.thisMonthEvents.push($scope.eventData.events[i]);
            }
        }
        
        
        for (var i = 0; i < $scope.weeks.length; i++) { //Перебераем недели 
            
            for (var j = 0; j < $scope.weeks[i].days.length; j++) { // Перебераем дни текущего календаря
                
                var currentDate = moment($scope.weeks[i].days[j].date).format("YYYY-MM-DD");
                for (k = 0; k < $scope.thisMonthEvents.length; k++) { // Перебераем события текущего месяца
                    var eventDate = moment($scope.thisMonthEvents[k].dateTime).format("YYYY-MM-DD");
                    if (currentDate === eventDate) { // Ищем совпадение по датам
                        $scope.weeks[i].days[j].events.push($scope.thisMonthEvents[k]); //Добавляем event в объект совпадающего дня
                    };
                };
                
            };
            
        };
    };
    // Конец функции внесения событий в объекты дней
    
    
    
    
    
    
    //  Страница выбранного дня
    $scope.select = function(day) { //срабатывает при выборе дня
        if (day.isCurrentMonth) {
        $scope.gridHours = [00, 01, 02, 03, 04, 05, 06, 07, 08, 09, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]; // Часы для таблицы событий
        
        $scope.selectedDay = day; 
        if ($scope.selectedDay.events.length === 0) {
            $scope.thereIsNoEvents = true;
        } else {
            $scope.thereIsNoEvents = false;
        }
        $scope.selectedDay.eventsHoursArray = [];//данные о событиях во временной переменной(часы и индекс события)
        $scope.selectedDay.thereIsFullDayEvent = false;// событие на весь день
        var eventHour;// будет брать строковые данные о часе события через метод moment()
        for (var i = 0; i < $scope.selectedDay.events.length; i++) {//Перебераем события выбранного дня
            if ($scope.selectedDay.events[i].fullDay === true) {
                // вносим данные о событии на весь день
                $scope.selectedDay.thereIsFullDayEvent = true;
                $scope.selectedDay.fullDayEventIndex = i;
            } else { 
                // вносим данные событиях во временную переменную(индекс и час события)
                $scope.selectedDay.events[i].time = moment($scope.selectedDay.events[i].dateTime).format("HH:mm"); // Часы и минуты события
                eventHour = moment($scope.selectedDay.events[i].dateTime).hour();
                $scope.selectedDay.eventsHoursArray.push({index: i, eventHour});
            };
            
        };
        $scope.eventHeader = moment($scope.selectedDay.date).format("Do MMMM");// заголовок с назваием дня
        $scope.backToCalendar = function() {
            $scope.showCalendar = true;
            $scope.showEventsPage = false;
        };
        
        
        $scope.getEvents = function(hour) {// Функция возврата данных о событиях отображаемого часа
            var thisHourEvents = [];//хранит объекты с индексами нужного часа
            for (var i = 0; i < $scope.selectedDay.eventsHoursArray.length; i++) {
                if (hour === $scope.selectedDay.eventsHoursArray[i].eventHour) {
                    thisHourEvents.push($scope.selectedDay.eventsHoursArray[i]);
                }
            };
            return thisHourEvents;
        };
        
        
        
            
        // Функция добавления нового события
        $scope.addNewEvent = function() {
            $scope.newEventCancel = function() {
                $scope.showEventsPage = true;
                $scope.showNewEventPage = false;
            };
            $scope.dateIsBeforeCurrentDay = false;
            $scope.isDateBefore = moment().isAfter($scope.selectedDay.date, 'day'); // сравниваем дату выбранного дня с текущей
            if ( $scope.isDateBefore ) {
                $scope.dateIsBeforeCurrentDay = true;
            } else {
            $scope.newEvent = {};
            $scope.newEvent.description = "";
            $scope.newEvent.fullDay = false;
            $scope.newFormSubmit = function() {
                $scope.newEventHours = moment($scope.newEvent.time).format('H');
                $scope.newEventMinutes = moment($scope.newEvent.time).format('m');
                $scope.newEvent.dateTime = moment($scope.selectedDay.date).startOf('day').add($scope.newEventHours, 'hours').add($scope.newEventMinutes, 'minutes'); // от начала дня прибавляем часы и минуты.
                $scope.isTimeBefore =  moment().isAfter($scope.newEvent.dateTime, 'hour'); // сравниваем время нового события с текущим временем.
                if ($scope.isTimeBefore) {
                    $scope.dateIsBeforeCurrentDay = true;
                } else {
                
                $scope.eventData.events.push( {international: false, fullDay: $scope.newEvent.fullDay, title: $scope.newEvent.title,  description: $scope.newEvent.description, dateTime: $scope.newEvent.dateTime}); // Добавляем в массив с событиями
                
                $scope.localSync(); // Синхронизируем с LocaStorage
                $scope.buildMonth($scope.start, $scope.month); // обновляем данные календаря
                $scope.pushEvents(); // вносим события в оъекты дней
                
                $scope.showCalendar = true;
                $scope.showNewEventPage = false;
                };
            };
                
            };
            
            $scope.showEventsPage = false;
            $scope.showNewEventPage = true;
        };
        
        
        //Функция открытия блока с описанием и редактированием
        $scope.eventsDetails = function(hour) {
            $scope.eventsToOpen = [];
            if ( $scope.getEvents(hour).length > 0 ) {
            $scope.eventsToOpen = $scope.getEvents(hour); // массив данных о редактируемых событиях
            $scope.eventIndex = function(eventToFind) { //Ищем идекс события в массиве всех событий
                for (var i = 0; i < $scope.eventData.events.length; i++) {
                    if ( eventToFind.dateTime === $scope.eventData.events[i].dateTime ) {
                        return i;
                    };
                };
            };
            
            $scope.editEvent = function(eventToEdit) {
                eventToEdit.editForm = !eventToEdit.editForm;
            };
            
            
            for (var i = 0; i < $scope.eventsToOpen.length; i++) {
                $scope.eventsToOpen[i].editForm = false;
                $scope.eventsToOpen[i].globalIndex =  $scope.eventIndex($scope.selectedDay.events[$scope.eventsToOpen[i].index]);
            };
            
            $scope.editSubmit = function(editedDay, editedDayIndex) {
                $scope.eventData[editedDayIndex] = {international: false, fullDay: false, title: editedDay.title, description: editedDay.description, dateTime: editedDay.dateTime, time: editedDay.time};
                
                $scope.localSync(); // Синхронизируем с LocaStorage
                $scope.buildMonth($scope.start, $scope.month); // обновляем данные календаря
                $scope.pushEvents(); // вносим события в оъекты дней
                
                $scope.showEditPage = false;
                $scope.showCalendar = true;
                
                
            };
            
            $scope.deleteEvent = function(deleteDayIndex) {
                $scope.eventData.events.splice(deleteDayIndex, 1);
                
                $scope.localSync(); // Синхронизируем с LocaStorage
                $scope.buildMonth($scope.start, $scope.month); // обновляем данные календаря
                $scope.pushEvents(); // вносим события в оъекты дней
                
                $scope.showEditPage = false;
                $scope.showCalendar = true;
            };
            
            
            $scope.editPageBack = function() {
                $scope.showEventsPage = true;
                $scope.showEditPage = false;
            };
            
            $scope.showEventsPage = false;
            $scope.showEditPage = true;
            
        } else if ( $scope.selectedDay.events[$scope.selectedDay.fullDayEventIndex]) {
            
            // если событие на целый день
            $scope.fullDayEditPageBack = function() {
                $scope.showEventsPage = true;
                $scope.showFullDayEditPage = false;
            };
            
            for (var i = 0; i < $scope.eventData.events.length; i++ ) {//Находим индекс события в массиве всех событий
                if ($scope.eventData.events[i].dateTime === $scope.selectedDay.events[$scope.selectedDay.fullDayEventIndex].dateTime) {
                    $scope.selectedFullDayEventIndex = i;
                };
            };
            
            
            $scope.fullDayEventSubmit = function(fullDayToSubmit) {
                $scope.eventData.events[$scope.selectedFullDayEventIndex].title = fullDayToSubmit.title;
                $scope.eventData.events[$scope.selectedFullDayEventIndex].description = fullDayToSubmit.description;
                
                $scope.localSync(); // Синхронизируем с LocaStorage
                $scope.buildMonth($scope.start, $scope.month); // обновляем данные календаря
                $scope.pushEvents(); // вносим события в оъекты дней
                
                $scope.showFullDayEditPage = false;
                $scope.showCalendar = true;
            };
            
            $scope.fullDayEventDelete = function() {
                $scope.eventData.events.splice($scope.selectedFullDayEventIndex, 1);
                
                $scope.localSync(); // Синхронизируем с LocaStorage
                $scope.buildMonth($scope.start, $scope.month); // обновляем данные календаря
                $scope.pushEvents(); // вносим события в оъекты дней
                
                $scope.showFullDayEditPage = false;
                $scope.showCalendar = true;
            }
            
            
            $scope.showEventsPage = false;
            $scope.showFullDayEditPage = true;
        };         // else if end
        };
        
        
        $scope.showCalendar = false;
        $scope.showEventsPage = true;
        
        };
    };
//     Конец функций выбора дня
    
    
    
    
    
    $scope.buildMonth($scope.start, $scope.month);
    $scope.pushEvents();
});